----------------------------------------------------My OPENCV EXAMPLES------------------------------------------------------------------
INTRODUCTION: 
----------------------------------------------------------------------------------------------------------------------------------
This repository aims to learn some basic concepts such as filters (Kernels), morphological operations and another important processing images: 

The objetive is to compare the algorithm developed by myself and the opencv Algorithm.

FRECUENCY FILTERS
- FOURIER DISCRETE TRANSFORMATION
- GAMMA REDUCTION

 SPATIAL FILTERS
- LAPLACE OPERATOR (Sharpening)
- SOBEL DERIVATE
- Fuzzy operator
-
PREREQUESITES: 
- Computer vision notions.
- OpenCV basic knowledge. 
- CMake basic knowledge
