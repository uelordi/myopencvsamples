#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <vector>
//#include "opencv2/video/tracking.hpp"
//include <Windows.h>
#include <math.h>

using namespace cv;
using namespace std;
int K;
int N;
int S;

struct Cluster {
  cv::Point center;
};
vector <Cluster > m_ccenters;
int main( )
{
  std::string path("/Users/uelordi/Repos/myopencvsamples/Img/lena.jpg");
 K=10;
 Mat input_img=cv::imread(path,-1);
 Mat imgGray;
 std::cout<<"num_channels-> "<<input_img.channels()<<endl;
 std::cout<<"sizes: -> ["<<input_img.cols<<","<<input_img.rows<<"]"<<endl;
 if( input_img.rows>0 && input_img.cols>0 )
 {
   cv::cvtColor(input_img,imgGray,cv::COLOR_BGR2GRAY);
   N=imgGray.rows*imgGray.cols;
   S=sqrt(N/K);
   cout<< "[K,N,S]->["<<K<<","<<N<<","<<S<<"]"<<endl;
   int numCols=(int)imgGray.cols/S;
   int numRows=(int)imgGray.rows/S;
   cout<<"numCols:"<<numCols<<endl;
   cout<<"numRows:"<<numRows<<endl;

   int colsumer=0;
   int rowsumer=0;

   for(size_t i=0;i<numRows;i++)
   {
     rowsumer=rowsumer+S;
     colsumer=S;
     for(size_t j=0;j<numCols;j++)
     {
        Cluster aux;
        aux.center.x=colsumer/2;
        aux.center.y=rowsumer/2;
        m_ccenters.push_back(aux);
        cv::circle(input_img, aux.center,3, Scalar(255,0,0),CV_FILLED, 8,0);
        cv::circle(input_img,cv::Point(colsumer,rowsumer) ,3, Scalar(0,2*i,0),CV_FILLED, 8,0);
        std::cout<<"Point ["<<colsumer<<","<<rowsumer<<"]"<<endl;
        colsumer=colsumer+S;

     }
   }
   //try to draw the first vision of the image
  /* for(size_t i=0;i<K;i++)
   {
  	 int value=(i+1)*S;
  	 m_ccenters.push_back(value);
   }*/
  cv::namedWindow("Result");
  cv::imshow("Result",input_img);
  cv::waitKey();
 }

return 0;
}
