#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <vector>
#include <math.h>
using namespace cv;
using namespace std;
int main( )
{
 std::string path("/home/VICOMTECH/uelordi/repos/myopencvsamples/Resources/test.png");
 Mat input_img=cv::imread(path,-1);
 std::cout<<"num_channels-> "<<input_img.channels()<<endl;
 std::cout<<"sizes: -> ["<<input_img.cols<<","<<input_img.rows<<"]"<<endl;
 cv::Mat mask;

 if( input_img.rows>0 && input_img.cols>0 )
 {
    cv::inRange(input_img, cv::Scalar(0.0, 0.0, 219), cv::Scalar(255, 255, 255),mask);
 }
  cv::namedWindow("Result");
  cv::imshow("Result",input_img);
  cv::imshow("Mask",mask);
  cv::waitKey();
  return 0;
}
